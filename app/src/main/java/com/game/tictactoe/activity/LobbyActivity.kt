package com.game.tictactoe.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.game.tictactoe.R
import com.game.tictactoe.general.*
import com.game.tictactoe.general.SocketMessage.*
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import kotlinx.android.synthetic.main.lobby_activity.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.random.Random

class LobbyActivity : AppCompatActivity() {

    private val socket: Socket by lazy(LazyThreadSafetyMode.NONE) { IO.socket(SOCKET_URL) }
    private val lobbyData: LobbyData by lazy(LazyThreadSafetyMode.NONE) {
        intent.getSerializableExtra(
            LOBBY_DATA
        ) as LobbyData
    }
    private lateinit var playerNames: JSONArray

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.lobby_activity)
        addButtonsClickListeners()
        listenMessages()
        socket.connect()
        emitEvents()
    }

    private fun addButtonsClickListeners() {
        closeRoom.setOnClickListener {
            val intent = Intent(this, MenuActivity::class.java)
//            socket.emit(CLOSE_ROOM.toString(), lobbyData.lobbyCode)
            startActivity(intent)
        }

        startGame.setOnClickListener {
            val room = JSONObject()
            room.put("roomCode", lobbyData.lobbyCode)

            when (Random.nextBoolean()) {
                true -> {
                    room.put("playerOne", playerNames[0])
                    room.put("playerTwo", playerNames[1])
                }
                false -> {
                    room.put("playerOne", playerNames[1])
                    room.put("playerTwo", playerNames[0])
                }
            }

            socket.emit(START_GAME.toString(), room)
        }
    }

    private fun listenMessages() {
        socket.on(JOINED_ROOM.toString()) {
            val room = it[0] as JSONObject
            playerNames = room.getJSONArray("players")
            lobbyData.lobbyCode = room.getString("code")
            runOnUiThread {
                roomCode.text = getString(R.string.room_code, lobbyData.lobbyCode)
                playerOneLobbyName.text =
                    getString(R.string.player_connected_to_lobby, playerNames[0])

                if (playerNames[1] == "") {
                    playerTwoLobbyName.text = getString(R.string.waiting_for_player_2)
                    startGame.isClickable = false
                } else {
                    playerTwoLobbyName.text =
                        getString(R.string.player_connected_to_lobby, playerNames[1])
                    startGame.isClickable = true
                }
            }
        }

        socket.on(START_GAME.toString()) {
            val players = it[0] as JSONObject

            val intent = Intent(this, OnlineGameActivity::class.java).apply {
                putExtra(
                    PLAYERS,
                    Room(
                        lobbyData.lobbyCode,
                        players.getString("playerOne"),
                        players.getString("playerTwo"),
                        lobbyData.localPlayerName
                    )
                )
            }
            socket.disconnect()
            startActivity(intent)
        }
    }

    private fun emitEvents() {
        if (lobbyData.lobbyType == LobbyType.CREATE_LOBBY) {
            socket.emit(CREATE_AND_JOIN_ROOM.toString(), lobbyData.localPlayerName)
        } else {
            socket.emit(
                JOIN_ROOM.toString(),
                JSONObject(
                    Gson().toJson(
                        LobbyPlayer(
                            lobbyData.localPlayerName,
                            lobbyData.lobbyCode
                        )
                    )
                )
            )
        }
    }
}