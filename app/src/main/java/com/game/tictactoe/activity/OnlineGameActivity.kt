package com.game.tictactoe.activity

import android.graphics.Typeface
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.game.tictactoe.R
import com.game.tictactoe.general.*
import com.game.tictactoe.general.GameStatus.*
import com.game.tictactoe.general.SocketMessage.REJOIN_ROOM
import com.game.tictactoe.general.SocketMessage.UPDATE_GAME_STATE
import com.github.nkzawa.socketio.client.IO
import com.github.nkzawa.socketio.client.Socket
import com.google.gson.Gson
import kotlinx.android.synthetic.main.offline_game_activity.*
import org.json.JSONObject

class OnlineGameActivity : AppCompatActivity() {

    private val socket: Socket by lazy(LazyThreadSafetyMode.NONE) { IO.socket(SOCKET_URL) }
    private val boxes: List<Button> by lazy(LazyThreadSafetyMode.NONE) {
        listOf(box1, box2, box3, box4, box5, box6, box7, box8, box9)
    }
    private val room: Room by lazy(LazyThreadSafetyMode.NONE) {
        intent.getSerializableExtra(
            PLAYERS
        ) as Room
    }
    private var isGameOver = false
    private val playerOne: String by lazy(LazyThreadSafetyMode.NONE) {
        room.playerOne
    }
    private val playerTwo: String by lazy(LazyThreadSafetyMode.NONE) {
        room.playerTwo
    }
    private lateinit var currentPlayer: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.offline_game_activity)
        resetGame()
        listenMessages()
        socket.connect()
        rejoinRoom()
        addClickListeners()
    }

    private fun addClickListeners() {
        val boxListener = View.OnClickListener { v ->
            val box = v as Button
            if (box.text.isEmpty()) {
                if (currentPlayer == room.localPlayerName) {
                    when (currentPlayer) {
                        playerOne -> {
                            box.text = X
                        }
                        playerTwo -> {
                            box.text = O
                        }
                    }
                    updateGameState()
                }
            }
        }

        boxes.forEach {
            it.setOnClickListener(boxListener)
        }

        restartGame.setOnClickListener {
            resetGame()
        }
    }

    private fun rejoinRoom() {
        socket.emit(REJOIN_ROOM.toString(), room.roomCode)
    }

    private fun listenMessages() {
        socket.on(UPDATE_GAME_STATE.toString()) { data ->
            val gameState = Gson().fromJson(data[0].toString(), GameState::class.java)

            runOnUiThread {
                boxes.forEachIndexed { index, box ->
                    box.text = gameState.gameBoard[index]
                }
            }

            when (gameState.status) {
                IN_PROGRESS.value -> switchTurn(gameState.nextPlayer)
                else -> gameOver(gameState.status)
            }
        }
    }

    private fun switchTurn(nextPlayer: String) {
        currentPlayer = nextPlayer
        runOnUiThread {
            gameInfo.text = getString(R.string.player_turn, nextPlayer)
            if (playerNameLeft.text.contains(currentPlayer)) {
                playerNameLeft.setTypeface(null, Typeface.BOLD)
                playerNameRight.setTypeface(null, Typeface.NORMAL)
            } else {
                playerNameLeft.setTypeface(null, Typeface.NORMAL)
                playerNameRight.setTypeface(null, Typeface.BOLD)
            }
        }
    }

    private fun gameOver(status: Int) {
        runOnUiThread {
            when (status) {
                PLAYER_ONE_WON.value -> gameInfo.text =
                    getString(R.string.win_message, playerOne)
                PLAYER_TWO_WON.value -> gameInfo.text =
                    getString(R.string.win_message, playerTwo)
                EQUALITY.value -> gameInfo.text = getString(R.string.equality)
            }
        }
        boxes.forEach {
            it.isClickable = false
        }
        isGameOver = true
    }

    private fun resetGame() {
        isGameOver = false

        boxes.forEach {
            it.text = ""
            it.isClickable = true
        }

        when (room.localPlayerName) {
            playerOne -> {
                playerNameLeft.text = playerOne
                playerNameLeft.setTypeface(null, Typeface.BOLD)
                playerNameRight.text = playerTwo
            }
            playerTwo -> {
                playerNameLeft.text = playerTwo
                playerNameRight.setTypeface(null, Typeface.BOLD)
                playerNameRight.text = playerOne
            }
        }
        currentPlayer = playerOne
        gameInfo.text = getString(R.string.player_turn, playerOne)
    }

    private fun updateGameState() {
        val gameState = GameState(room.roomCode)
        gameState.gameBoard = boxes.map { it.text.toString() }
        gameState.status = getGameStatus(getGameBoardLines())

        if (gameState.status == IN_PROGRESS.value) {
            gameState.nextPlayer = when (currentPlayer) {
                playerOne -> playerTwo
                else -> playerOne
            }
        }

        socket.emit(UPDATE_GAME_STATE.toString(), convertToJSONObject(gameState))
    }

    private fun getGameBoardLines(): List<String> {
        val lineOneHorizontal =
            boxes[0].text.toString() + boxes[1].text.toString() + boxes[2].text.toString()
        val lineTwoHorizontal =
            boxes[3].text.toString() + boxes[4].text.toString() + boxes[5].text.toString()
        val lineThreeHorizontal =
            boxes[6].text.toString() + boxes[7].text.toString() + boxes[8].text.toString()

        val lineOneVertical =
            boxes[0].text.toString() + boxes[3].text.toString() + boxes[6].text.toString()
        val lineTwoVertical =
            boxes[1].text.toString() + boxes[4].text.toString() + boxes[7].text.toString()
        val lineThreeVertical =
            boxes[2].text.toString() + boxes[5].text.toString() + boxes[8].text.toString()

        val mainDiagonal =
            boxes[0].text.toString() + boxes[4].text.toString() + boxes[8].text.toString()
        val secondaryDiagonal =
            boxes[2].text.toString() + boxes[4].text.toString() + boxes[6].text.toString()

        return listOf(
            lineOneHorizontal,
            lineTwoHorizontal,
            lineThreeHorizontal,
            lineOneVertical,
            lineTwoVertical,
            lineThreeVertical,
            mainDiagonal,
            secondaryDiagonal
        )
    }

    private fun getGameStatus(lines: List<String>): Int {
        for (line in lines) {
            if (line == X_WINNING_COMBINATION) {
                return PLAYER_ONE_WON.value
            } else if (line == O_WINNING_COMBINATION) {
                return PLAYER_TWO_WON.value
            }
        }

        if (boxes.none { it.text.isEmpty() }) {
            return EQUALITY.value
        }

        return IN_PROGRESS.value
    }

    private fun convertToJSONObject(objectToConvert: Any): JSONObject {
        return JSONObject(Gson().toJson(objectToConvert))
    }
}