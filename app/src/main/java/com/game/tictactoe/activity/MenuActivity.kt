package com.game.tictactoe.activity

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.contains
import com.game.tictactoe.R
import com.game.tictactoe.general.LOBBY_DATA
import com.game.tictactoe.general.LobbyData
import com.game.tictactoe.general.LobbyType
import kotlinx.android.synthetic.main.menu_activity.*

class MenuActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.menu_activity)
        createGameLobby.setOnClickListener { openCreateGameLobbyDialog() }
        joinGame.setOnClickListener { openJoinGameDialog() }
        playSinglePlayer.setOnClickListener { openPlaySinglePlayerDialog() }
    }

    private fun openCreateGameLobbyDialog() {
        val layout = LinearLayout(this)
        layout.orientation = LinearLayout.VERTICAL

        val playerName = EditText(this)
        playerName.hint = "Enter player name.."
        layout.addView(playerName)

        val dialogBuilder = AlertDialog.Builder(this)

        dialogBuilder
            .setView(layout)
            .setPositiveButton(
                "Ok"
            ) { _, _ ->
                val playerNameText =
                    if (playerName.text.isEmpty()) "Player 1" else playerName.text.toString()
                goToLobbyActivity(playerNameText, LobbyType.CREATE_LOBBY)
            }
            .setNegativeButton(
                "Cancel", null
            )
            .create()
            .show()
    }

    private fun openJoinGameDialog() {
        val layout = LinearLayout(this)
        layout.orientation = LinearLayout.VERTICAL

        val lobbyCode = EditText(this)
        lobbyCode.hint = "Enter lobby code.. (e.g. B4F9)"
        layout.addView(lobbyCode)

        val playerName = EditText(this)
        playerName.hint = "Enter player name.."
        layout.addView(playerName)

        val warningMessage = TextView(this)
        warningMessage.text = "A lobby code is needed to enter the game!"

        val dialogBuilder = AlertDialog.Builder(this)

        val dialog = dialogBuilder
            .setView(layout)
            .setPositiveButton(
                "Ok", null
            )
            .setNegativeButton(
                "Cancel", null
            )
            .create()

        dialog.show()

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener {
            if (lobbyCode.text.isNotEmpty()) {
                val playerNameText =
                    if (playerName.text.isEmpty()) "Player 2" else playerName.text.toString()
                dialog.dismiss()
                goToLobbyActivity(
                    playerNameText,
                    LobbyType.JOIN_LOBBY,
                    lobbyCode.text.toString()
                )
            } else if (!layout.contains(warningMessage) && lobbyCode.text.isEmpty()) {
                layout.addView(warningMessage)
            }
        }
    }

    private fun openPlaySinglePlayerDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val message = TextView(this)
        message.text = "Not Implemented Yet.."

        dialogBuilder
            .setView(message)
            .create()
            .show()
    }

    private fun goToLobbyActivity(
        playerName: String,
        lobbyType: LobbyType,
        lobbyCode: String = ""
    ) {
        val intent = Intent(this, LobbyActivity::class.java).apply {
            putExtra(
                LOBBY_DATA,
                LobbyData(playerName, lobbyType, lobbyCode)
            )
        }
        startActivity(intent)
    }
}