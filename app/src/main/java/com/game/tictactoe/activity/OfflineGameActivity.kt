package com.game.tictactoe.activity

import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.game.tictactoe.general.O_WINNING_COMBINATION
import com.game.tictactoe.R
import com.game.tictactoe.general.X_WINNING_COMBINATION
import kotlinx.android.synthetic.main.offline_game_activity.*

class OfflineGameActivity : AppCompatActivity() {

    private val playerOne = "X"
    private val playerTwo = "O"
    private val boxes by lazy(LazyThreadSafetyMode.NONE) {
        listOf(box1, box2, box3, box4, box5, box6, box7, box8, box9)
    }
    private var currentPlayer = playerOne
    private var isGameOver = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.offline_game_activity)

        gameInfo.text = getString(R.string.player_turn, currentPlayer)
        restartGame()

        val boxListener = View.OnClickListener { v ->
            val box = v as Button
            if (box.text.isEmpty()) {
                when (currentPlayer) {
                    playerOne -> box.text =
                        playerOne
                    playerTwo -> box.text =
                        playerTwo
                }
                updateGameStatus()
            }
        }

        boxes.forEach {
            it.setOnClickListener(boxListener)
        }

        restartGame.setOnClickListener {
            restartGame()
        }
    }

    private fun restartGame() {
        isGameOver = false

        boxes.forEach {
            it.text = ""
            it.isClickable = true
        }

        currentPlayer = playerOne
        gameInfo.text = getString(R.string.player_turn, currentPlayer)
    }

    private fun updateGameStatus() {
        val lineOneHorizontal =
            boxes[0].text.toString() + boxes[1].text.toString() + boxes[2].text.toString()
        val lineTwoHorizontal =
            boxes[3].text.toString() + boxes[4].text.toString() + boxes[5].text.toString()
        val lineThreeHorizontal =
            boxes[6].text.toString() + boxes[7].text.toString() + boxes[8].text.toString()

        val lineOneVertical =
            boxes[0].text.toString() + boxes[3].text.toString() + boxes[6].text.toString()
        val lineTwoVertical =
            boxes[1].text.toString() + boxes[4].text.toString() + boxes[7].text.toString()
        val lineThreeVertical =
            boxes[2].text.toString() + boxes[5].text.toString() + boxes[8].text.toString()

        val mainDiagonal =
            boxes[0].text.toString() + boxes[4].text.toString() + boxes[8].text.toString()
        val secondaryDiagonal =
            boxes[2].text.toString() + boxes[4].text.toString() + boxes[6].text.toString()

        val lines = listOf(
            lineOneHorizontal,
            lineTwoHorizontal,
            lineThreeHorizontal,
            lineOneVertical,
            lineTwoVertical,
            lineThreeVertical,
            mainDiagonal,
            secondaryDiagonal
        )

        lines.forEach {
            when (it) {
                X_WINNING_COMBINATION -> {
                    gameInfo.text = "Player X won!"
                    isGameOver = true
                }
                O_WINNING_COMBINATION -> {
                    gameInfo.text = "Player O won!"
                    isGameOver = true
                }
            }
        }

        if (!isGameOver && boxes.none { it.text.isEmpty() }) {
            gameInfo.text = "Equality!"
            isGameOver = true
        }


        if (isGameOver) {
            boxes.forEach {
                it.isClickable = false
            }
        } else {
            switchTurn()
        }
    }

    private fun switchTurn() {
        when (currentPlayer) {
            playerOne -> currentPlayer =
                playerTwo
            playerTwo -> currentPlayer =
                playerOne
        }
        gameInfo.text = getString(R.string.player_turn, currentPlayer)
    }
}
