package com.game.tictactoe.general

import java.io.Serializable

class LobbyData(
    val localPlayerName: String,
    val lobbyType: LobbyType,
    var lobbyCode: String = ""
) : Serializable

class LobbyPlayer(val name: String, val roomCode: String) :
    Serializable

class Room(
    val roomCode: String,
    val playerOne: String,
    val playerTwo: String,
    val localPlayerName: String
) : Serializable

data class GameState(val roomCode: String) : Serializable {
    var gameBoard: List<String> = emptyList()
    var status = -1
    var nextPlayer = ""
}