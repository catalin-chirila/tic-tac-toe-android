package com.game.tictactoe.general

const val SOCKET_URL = "https://tic-tac-toe-cc.herokuapp.com"
const val PLAYERS = "PLAYERS"
const val LOBBY_DATA = "lobbyData"
const val X = "X"
const val O = "O"
const val X_WINNING_COMBINATION = "XXX"
const val O_WINNING_COMBINATION = "OOO"

enum class LobbyType {
    CREATE_LOBBY, JOIN_LOBBY
}

enum class SocketMessage {
    CREATE_AND_JOIN_ROOM,
    JOIN_ROOM,
    JOINED_ROOM,
    REJOIN_ROOM,
    CLOSE_ROOM,
    START_GAME,
    UPDATE_GAME_STATE
}

enum class GameSymbol(val value: String) {
    X("X"), O("O"), X_WINNING_COMBINATION("XXX"), O_WINNING_COMBINATION("OOO")
}

enum class GameStatus(val value: Int) {
    IN_PROGRESS(-1), EQUALITY(0), PLAYER_ONE_WON(1), PLAYER_TWO_WON(2)
}